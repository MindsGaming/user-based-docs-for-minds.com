# User Based Docs For Minds.com

To make a user based document outlining Minds.com relating to features, how-to, open source code and open source standards.

## Chapters: 
The chapters covered in this document are outlined below, you can add chapters or add on to chapters on the git for this project. To add to a chapter add a lettered value to the chapter below the last line of the current chapter, so if you wanted to add to “Chapter-1 Intro” you would label the new chapter “Chapter-1a {name}”. 


# [Chapter-1 Intro ](Chapter-1_Intro.MD)
- About Minds.Com


# [Chapter-2 Creating an Account](Chapter-2_Creating_an_Account.MD)
- How to create an account on Minds.com

# [Chapter-3 Features](Chapter-3_Features.MD)
- Website features for Minds.com

[-Chapter-3A Blogs](Chapter-3_Features.MD#chapter-3a-blogs)

[-Chapter-3B Videos](Chapter-3_Features.MD#chapter-3b-videos)

[-Chapter-3C Images](Chapter-3_Features.MD#chapter-3c-images)

[-Chapter-3D Groups](Chapter-3_Features.MD#chapter-3d-groups)

 [-Chapter-3E Messenger](Chapter-3_Features.MD#chapter-3e-messenger)

 [-Chapter-3F Video Chat](Chapter-3_Features.MD#chapter-3f-video-chat)

 [-Chapter-3G Conversations](Chapter-3_Features.MD#chapter-3g-conversations)

 [-Chapter-3H Blocking](Chapter-3_Features.MD#chatper-3h-blocking)

[-Chapter-3I Boosting](Chapter-3_Features.MD#chapter-3i-boosting)

[-Chapter-3J Pinned Posts](Chapter-3_Features.MD#chapter-3j-pinned-posts)


# [Chapter-4 Installing](Installing_Minds.MD)
- Installing Minds.com


# [Chapter-5 Docs](docs.MD)
- User based docs on:

[-Decentralized](docs.MD#decentralized)

[-Community Owned](docs.MD#community-owned)

[-Open Sourced](docs.MD#open-sourced)

[-Open Development](docs.MD#open-development)

[-Free Speech](docs.MD#free-speech)

[-ORG](docs.MD#org)

[-Missing Docs & Info](docs.MD#missing-docs-info)


# [Chapter-6 Tokens](Tokens.MD)
- Minds ERC-20 Token

[-Chapter-6A Setting up you wallet. (MetaMask)](Tokens.MD#setting-up-you-wallet-metamask)

[-Chapter-6B ERC-20 Tokens](Tokens.MD#erc-20-tokens)

[-Chapter-6C Rewards](Tokens.MD#rewards)

[-Chapter-6D Paraphrase](Tokens.MD#paraphrase)

[-Chapter-6E Boost](Tokens.MD#boost)

[-Chapter-6F Wire](Tokens.MD#wire) 

[-Chapter-6G Token Demand](Tokens.MD#token-demand)

[-Chapter-6H Minds Token Documentation](Tokens.MD#minds-token-documentation)


# Download & Hosting Docs

To run these docs as a website simply download the zip and unpack the site.

You can also [Remix On Glitch](https://glitch.com/edit/#!/remix/user-docs?utm_source=user-docs&utm_medium=button&utm_campaign=glitchButton)